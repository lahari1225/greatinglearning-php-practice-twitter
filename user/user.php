<?php
require 'config/config.php';
require 'config/db.php';

// Create Query
$query = "SELECT * FROM tweets WHERE username='{$_GET['name']}' ORDER BY slno DESC";

// Get Result
$result = mysqli_query($conn, $query);

// Fetch Data
$posts = mysqli_fetch_all($result, MYSQLI_ASSOC);

$query1 = "SELECT follow FROM follows WHERE username='{$_GET['name']}'";
$result1 = mysqli_query($conn, $query1);
$followees = mysqli_fetch_all($result1, MYSQLI_ASSOC);
$follows = array();
foreach ($followees as $followee):
 $followername = $followee['follow'];

 $query2 = "SELECT * FROM tweets WHERE username='{$followername}'";
 $result2 = mysqli_query($conn, $query2);
 $followers = mysqli_fetch_all($result2, MYSQLI_ASSOC);
 foreach ($followers as $follower):
  $follows[] = $follower;
 endforeach;
endforeach;
usort($follows, function ($item1, $item2) {
 return $item2['slno'] <=> $item1['slno'];
});
// Free Result
mysqli_free_result($result);
mysqli_free_result($result1);
mysqli_free_result($result2);

// Close Connection
mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="user.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Yatra One">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Gloria Hallelujah">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<title>Tweety</title>
</head>
<body>

    <header>
        <a href="http://localhost/tweety/home/"><img src="../img/img_avatar.png" alt="Avatar" class="logo"></a>
        <p class="title">Twitte</p>
        <p class="username"><?php echo $_GET['name'] ?></p>
        <button class="add-user" onclick="document.getElementById('id02').style.display='block'" style="width:auto;">Tweet</button>
        <button class="add-follow" onclick="document.getElementById('id03').style.display='block'" style="width:auto;">Follow</button>
    </header>
    <?php include 'tweet.php';?>
    <?php include 'follow.php';?>
    <div class="alltweets">
        <div id ="posts-container" class="container">
            <h3>Tweets</h3>
            <?php if ($posts == []): ?>
                <h4>You don't have any tweets, to start click on <strong style="color:rgb(80, 190, 226);">Tweet</strong> button above :)</h4>
                <?php else: ?>
                <div id="tweetbody">
                    <?php foreach ($posts as $post): ?>
                        <div class="well">
                            <div class="tweetpost">
                                <p>You: </p>
                                <h5><?php echo $post['tweet']; ?></h5>
                            </div>
                            <p>- <?php echo $post['interest']; ?></p>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
        <div id ="posts-container" class="container">
            <h3>Follower's Tweets</h3>
            <?php if ($follows == []): ?>
                <?php if ($followees == []): ?>
                    <h4>Your are not following anyone, to follow click on <strong style="color:rgb(80, 190, 226);">Follow</strong> button above :)</h4>
                <?php else: ?>
                    <h4>Your friends have nothing to tell you :(</h4>
                <?php endif;?>
                <?php else: ?>
                    <div id="tweetbody">
                        <?php foreach ($follows as $follow): ?>
                            <div class="well">
                                <div class="tweetpost">
                                    <p><?php echo $follow['username']; ?>: </p>
                                    <h5><?php echo $follow['tweet']; ?></h5>
                                </div>
                                <p>- <?php echo $follow['interest']; ?></p>
                            </div>
                        <?php endforeach;?>
                    </div>
            <?php endif;?>
        </div>I
    </div>

</body>
</html>