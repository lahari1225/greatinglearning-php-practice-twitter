<?php
require 'config/config.php';
require 'config/db.php';

// Check For Submit
if (isset($_POST['tweet'])) {
 // Get form data
 $uname = mysqli_real_escape_string($conn, $_GET['name']);
 $interest = mysqli_real_escape_string($conn, $_GET['interest']);
 $tweet = mysqli_real_escape_string($conn, $_POST['post']);

 $query = "INSERT INTO tweets(username, interest, tweet) VALUES('$uname', '$interest', '$tweet')";

 if (mysqli_query($conn, $query)) {
  header('Location: ' . ROOT_URL . "?name=$uname&interest=$interest");
 } else {
  echo 'ERROR: ' . mysqli_error($conn);
 }
}
mysqli_close($conn);
?>

<div id="id02" class="modal">

<form class="modal-content animate" action="<?php echo $_SERVER['PHP_SELF']; ?>?name=<?php echo $_GET['name']; ?>&interest=<?php echo $_GET['interest']; ?>" method="post">
  <div class="imgcontainer">
    <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
    <img src="../img/img_avatar.png" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <label for="post"><b>What's on your mind ;)</b></label>
    <textarea  class="form-control" placeholder="Write here.!" name="post" autocomplete="off" autofocus required></textarea><br>
    <button id="tweet" class="btn btn-success" type="submit" name="tweet">Tweet</button>
  </div>

</form>
</div>