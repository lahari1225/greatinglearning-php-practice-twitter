<?php
require 'config/config.php';
require 'config/db.php';

// Check For Submit
if (isset($_POST['submit'])) {
 // Get form data
 $uname = mysqli_real_escape_string($conn, $_POST['uname']);
 $interest = mysqli_real_escape_string($conn, $_POST['interest']);

 $query1 = "INSERT INTO users(username, interests) VALUES('$uname', '$interest')";

 if (mysqli_query($conn, $query1)) {
  header('Location: ' . ROOT_URL . '');
 } else {
  echo 'ERROR: ' . mysqli_error($conn);
 }
}
mysqli_close($conn);
?>

<div id="id01" class="modal">

<form class="modal-content animate" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
  <div class="imgcontainer">
    <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    <img src="../img/img_avatar.png" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <label for="uname"><b>Username</b></label>
    <input  class="form-control" type="text" placeholder="Enter Username" name="uname" autocomplete="off" autofocus required><br>

    <label for="interest"><b>Interests</b></label>
    <select name="interest" class="form-control" required>
          <option> Sports </option>
          <option> Movies </option>
          <option> Books </option>
          <option> Coding </option>
  </select><br>

    <button id="useradd" class="btn btn-success" type="submit" name="submit">Save</button>
  </div>

</form>
</div>

<script>
var modal = document.getElementById('id01');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>