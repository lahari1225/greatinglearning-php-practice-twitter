<?php
require 'config/config.php';
require 'config/db.php';

// Create Query
$query = 'SELECT * FROM users';

// Get Result
$result = mysqli_query($conn, $query);

// Fetch Data
$users = mysqli_fetch_all($result, MYSQLI_ASSOC);

// Free Result
mysqli_free_result($result);

// Close Connection
mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Yatra One">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Gloria Hallelujah">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<title>Twitter</title>
</head>
<body>
    <header>
        <img src="../img/img_avatar.png" alt="Avatar" class="logo">
        <p class="title">Twitte</p>
        <button class="add-user" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Add User</button>
    </header>
<?php include 'addusers.php';?>
<div class="home">
    <div id ="users-container" class="container">
    <?php foreach ($users as $user): ?>
        <div class="well">
            <img src="../img/profile.png" />
            <h4><?php echo $user['username']; ?></h4>
            <a id="more" class="btn btn-default" href="http://localhost/tweety/user/user.php?name=<?php echo $user['username']; ?>&interest=<?php echo $user['interests']; ?>">+ More</a>
            <?php include 'deleteuser.php'; ?>
        </div>
    <?php endforeach;?>
    </div>
    <div class="welcome-msg">
        <p>Know about,</p>
        <h1>What's happening in the world and what people are talking about right now.</h1>
    </div>
</div>

</body>
</html>