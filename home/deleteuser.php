<?php
require 'config/config.php';
require 'config/db.php';

// Check For Delete
if (isset($_POST['delete'])) {
 // Get ID
 $id = mysqli_real_escape_string($conn, $_POST['delete_id']);

 $query = 'DELETE FROM users WHERE id = 3';

 if (mysqli_query($conn, $query)) {
  echo 'Success';
  header('Location: ' . ROOT_URL . '');
 } else {
  echo 'ERROR: ' . mysqli_error($conn);
 }
}
mysqli_close($conn);
?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <input type="hidden" name="delete_id" value="<?php echo $user['id']; ?>">
    <button class="delete" name="delete"><span>&times;</span>
</form>